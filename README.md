/r/rangers
===============

SASS theme engine for r-rangers.

How to Implement
===============

  1. Compile app.scss ==> app.css
  2. Open app.css and remove the @charset from line one of the file.
  3. Copy the CSS and Paste into the subreddit stylesheet text area.
  4. Hit save!


Built on top of Naut
===============

Naut is a css theme you can use on reddit.com. It's free to use and any subreddit can use and edit it. Visit /r/Naut to preview the theme.
